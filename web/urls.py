from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from . import views

urlpatterns = [
    # region Base
    path('', views.IndexView.as_view(), name='index'),
    path('accounts/login/', LoginView.as_view(next_page='/'), name='login'),
    path('accounts/logout/', LogoutView.as_view(next_page='/'), name='logout'),
    path('accounts/registration/', views.CustomRegistrationView.as_view(), name='registration'),
    # endregion

    # region User
    path('user/detail/<int:pk>', views.UserDetailView.as_view(), name='user_detail'),
    path('user/update/<int:pk>', views.UserUpdateView.as_view(), name='user_update'),
    # endregion
]
