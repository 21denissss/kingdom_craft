from django.contrib.auth import models as auth_models
from django.db import models

# Create your models here.
from django.urls import reverse


def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.id, filename)


class CustomUser(auth_models.User):
    """
    Пользователь
    """
    name = models.CharField(
        verbose_name="Имя",
        max_length=1024,
        null=False, blank=False,
    )
    dob = models.DateField(
        verbose_name="Дата рождения",
        null=True, blank=True,
    )
    vk_url = models.CharField(
        verbose_name="Ссылка ВКонтакте",
        max_length=1024,
        null=True, blank=True
    )
    skin = models.ImageField(
        verbose_name="Скин персонажа",
        null=True, blank=True,
        upload_to=user_directory_path
    )

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('user_detail', args=(self.pk,))
