from django import forms
from django.contrib.auth.forms import UserCreationForm

from web.models import CustomUser


class CustomRegistrationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = CustomUser

        fields = (
            'username',
            'password1',
            'password2',
            'email',
            'vk_url',
            'skin',
        )


class UserForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = (
            'name',
            'dob',
            'vk_url',
            'skin'
        )
