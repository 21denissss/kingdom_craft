from django.urls import reverse
from django.views.generic import ListView, DetailView, UpdateView

from web.forms import UserForm
from web.models import CustomUser


class UserDetailView(DetailView):
    model = CustomUser
    template_name = "web/user/detail.html"


class UserUpdateView(UpdateView):
    model = CustomUser
    template_name = "web/user/update.html"
    form_class = UserForm

    @property
    def success_url(self):
        return reverse('user_detail', args=(self.kwargs['pk'],))
