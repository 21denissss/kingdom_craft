from .main import IndexView, CustomRegistrationView
from .user import UserDetailView, UserUpdateView
