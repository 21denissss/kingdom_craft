# Create your views here.
from django.views.generic import TemplateView
from registration.backends.simple.views import RegistrationView

from web.forms import CustomRegistrationForm


class IndexView(TemplateView):
    template_name = "web/index.html"


class CustomRegistrationView(RegistrationView):
    form_class = CustomRegistrationForm
    success_url = '/'
